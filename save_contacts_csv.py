import csv
from os.path import exists
def user_contacts_csv(user_details_list,poll_uuid):
    """_summary_

    Args:
        user_details_list (list): the user details list has contacts.csv user details 

    Returns:
        string: return response is showing success message.
    """
    field_names = ['UUID','Email',"Name","FamilyName"]
    users_list = [user_details_list]
    try:
        
        uuidsplict = users_list[0][0][:len(users_list[0][0])-1]
        if exists('polls/'+str(poll_uuid)+'/contacts.csv'):
            with open('polls/'+str(poll_uuid)+'/contacts.csv', mode="r") as csv_file:
                reader = csv.reader(csv_file) 
                i = 0
                for i in reader:
                    pass
                    i = 1
                if i != 0:
                    uuidsplict = users_list[0][0][:len(users_list[0][0])-1]
                    with open('polls/'+str(poll_uuid)+'/contacts.csv', 'a', encoding='UTF8', newline='') as f:
                        writer = csv.writer(f)
                        writer.writerows(users_list)
                else:
                    uuidsplict = users_list[0][0][:len(users_list[0][0])-1]
                    with open('polls/'+str(poll_uuid)+'/contacts.csv', 'w', encoding='UTF8', newline='') as f:
                        writer = csv.writer(f)
                        # write the header
                        writer.writerow(field_names)
                        # write multiple rows
                        writer.writerows(users_list)

    except Exception as e:
        uuidsplict = users_list[0][0][:len(users_list[0][0])-1]
        with open('polls/'+str(poll_uuid)+'/contacts.csv', 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            # write the header
            writer.writerow(field_names)
            # write multiple rows
            writer.writerows(users_list)
    return "success"

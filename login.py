import csv
from os.path import exists

# list of column names 
from flask import request


def user_login_csv(user_details):
    """Check login information."""
    if not exists("user.csv"):
        #print("user.csv does not exists")
        return {"status": -1}
    with open('user.csv', mode="r") as csv_file:
        user_csv_reader = csv.reader(csv_file) 
        for user_csv_data in user_csv_reader:
            if user_csv_data is not None and len(user_csv_data)>0:
                if user_csv_data[0] == user_details[0] or user_csv_data[2] == user_details[0]:
                    #print("user name:", user_details[0])
                    if str(user_csv_data[1]) == str(user_details[1]):
                        #print("user password:", user_details[1])
                        return {"status": 3}
                    else:
                        #print("wrong password")
                        return {"status": 2}
    return {"status": 1}


def user_register_csv(user_details):
    """_summary_

    Args:
        user_details (list): user details for registraion usename,password,name,phone_number

    Returns:
        dictionary: return status value 1,5 
        1 is successfully registration and 5 is already existed user.
    """
    field_names = ["Email","Password","Name","Phone"]
    if exists("user.csv"):
        with open('user.csv', mode="r") as csv_file:
            user_data_reader = csv.reader(csv_file)
            for user_data_csv in user_data_reader:
                if user_data_csv is not None and len(user_data_csv)>0:
                    if user_data_csv[0] == user_details[0]:
                        return {"status": 5}   # status 5 means email alreday registered
        with open('user.csv', 'a', encoding='UTF8') as f:
            writer = csv.writer(f)
            writer.writerow(user_details)
    else:
        with open('user.csv', 'w', encoding='UTF8') as f:
            writer = csv.writer(f)
            # write the header
            writer.writerow(field_names)
            # write multiple rows
            writer.writerow(user_details)
    return {"status": 1}   # status 1 mean registered successfully


def user_forgot_csv(dicts):
    """_summary_

    Args:
        dicts (list): user password forgot function in the list has user email and password 
        if email existed password will be change and if it is not exist return response message

    Returns:
        dictionary: return response status 3 value ,
        3 means password changed successfully.
    """
    # Define the file path
    csv_file = 'user.csv'
    # Read the CSV data into a list of dictionaries
    data = []
    with open(csv_file, 'r') as file:
        csv_reader = csv.DictReader(file)
        for row in csv_reader:
            # Make the necessary changes to the data
            # Assuming you want to update the email for a specific user
            if row['Email'] == dicts[0]: #checking the email validation
                #update the new password in user.csv file
                row['Password'] = dicts[1] 
            data.append(row) 
    # Write the updated data back to the CSV file
    with open(csv_file, 'w', newline='') as file:
        fieldnames = ['Email', 'Password', 'Name', 'Phone']
        csv_writer = csv.DictWriter(file, fieldnames=fieldnames)
        # Write the headers
        csv_writer.writeheader()
        # Write the updated data
        csv_writer.writerows(data)
    data = {"status":3}
    return data

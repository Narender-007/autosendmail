#importing for required packages
import csv
import sys
import re
from flask import *
import toml
from werkzeug.utils import secure_filename
import os
import smtplib, ssl
from flask import request
import tomli
import os
from save_contacts_csv import user_contacts_csv
from login import user_login_csv,user_register_csv,user_forgot_csv
import hashlib
import logging
import uuid


#Defined a flask name 
app = Flask(__name__)
app = Flask(__name__, template_folder='AdminLogin')

sender_script_filename = 'sendall.sh'
ssmtp_line_template = 'ssmtp {Address} < {EmailFName}'
send_line_template = ssmtp_line_template

#Creating a log file
logging.basicConfig(filename='service.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
logging.debug('This is a debug message')
logging.info('This is an info message')
logging.warning('This is a warning message')
logging.error('This is an error message')
logging.critical('This is a critical message')

#home page view function
@app.route("/", methods = ['GET','POST'])
def home():   
    return render_template("login.html")

@app.route("/forgot_password")
def forgot_password():
    return render_template("forgot_password.html")

@app.route("/registration",methods=['GET','POST'])
def registration():
    if request.method == "POST":
        name = request.form['name']
        phone = request.form['phone']
        email = request.form['email']
        user_password = request.form['password']
        # Declaring Password
        
        # Encoding the password
        #creating a password hashing
        hash_password = hashlib.md5(user_password.encode())

        # password is hexadigest
        user_list = [email, hash_password.hexdigest(), name, phone]
        user_response = user_register_csv(user_list)
        if user_response['status'] == 1:
            return render_template("login.html",message="registration is completed")
        elif user_response['status'] == 5:
            return render_template("login.html",message="email already registered")
    else:
        return render_template("registration.html")

@app.route("/forgotpassword",methods=['GET','POST'])
def forgotpassword():
    """_summary_
    users can change password using forgotpassword option
    user passing the registered email and enter new password and confirm password.
    will check email is existed or not if existed password will be changed successfully.
    if it is not exist showing response is invalid user name .
    Returns:
        _type_: _description_
    """
    if request.method == "POST":
        email = request.form['email'] #user input email
        passwordddd = request.form['password'] #user input password
        # Encoding the password
        hashed = hashlib.md5(passwordddd.encode()) #creating hash password

        # Printing the Hash
        user_details_list = [email,hashed.hexdigest()] 
        user_response_data = user_forgot_csv(user_details_list)
        
        if user_response_data['status'] == 1:
            return render_template("forgot_password.html",message="invalid user name")
        elif user_response_data['status'] == 2:
            return render_template("forgot_password.html",message="invalid user password")
        else:
            return render_template("login.html",message="message")
    else:
        return render_template("forgot_password.html")



@app.route("/index", methods = ['GET','POST'])
def index():
    """_summary_
    this is web poll login page and home page.
    user passing user name and password 
    username and password checking in user.csv file
    if above the details are matched web poll page redirected 
    if it is not matched above the details showing invalid password or username
    Returns:
        _type_: _description_
    """
    if request.method == 'POST':
        name = request.form['name'] #input username 
        password = request.form['password']#input password
        # Encoding the password
        hashed = hashlib.md5(password.encode())

        user_detail_list = [name, hashed.hexdigest()] 
        user_response_login = user_login_csv(user_detail_list)
        if user_response_login['status'] == 1:
            return render_template("login.html",message="invalid user name")
        elif user_response_login['status'] == 2:
            return render_template("login.html",message="invalid user password")
        else:
            return redirect("uploadfiles")
    else:
        return render_template("login.html")
@app.route("/uploadfiles")
def uploadfiles():
    return render_template("upload.html")


#user questions file
@app.route("/poll/<poll_id>/<user_id>")
def poll(poll_id, user_id, delimiter=',', quotechar='"'):
    """_summary_

    Args:
        poll_id (string): unique uuid full length
        user_id (string): unique uuid diffrentiate the users
        delimiter (str, optional): _description_. Defaults to ','.
        quotechar (str, optional): _description_. Defaults to '"'.

    Returns:
        _type_: _description_
    """
    questionsList = []
    user_identity_number = user_id[:len(user_id)-1]
    file_name = "pattern.toml"
    file_toml_path = f"polls/{poll_id}/{file_name}"   # creating a file name

    # toml file data from toml folder
    with open(file_toml_path, mode="rb") as fp:
        config = tomli.load(fp)

    if request.method != "POST":
        #print("POST")
        # checking the questions in a pattern
        #print(config)
        if len(config['questions']) > 0:
            return render_template("questions.html", poll_id_variable=poll_id, user_id=user_id, user_id_number=config, pknumber=len(config))
    elif request.method == "POST":
        #print("Not POST")
        ans = []
        if len(questionsList) > 0:
            for l in questionsList:
                value = l.question
                options = request.form[value]
                ans.append(options)
                return render_template("success.html")
    return render_template("upload.html")

# user questions and answers taking from particular pattern in toml file
@app.route("/question-submit/<pk1>/<user_id_number>", methods = ['GET','POST'])
def submitQuestions(pk1,user_id_number,delimiter=',', quotechar='"'):
    """_summary_

    Args:
        pk1 (string): unique uuid full length and find folder name 
        user_id_number (_type_): unique uuid for differentiate users
        delimiter (str, optional): _description_. Defaults to ','.
        quotechar (str, optional): _description_. Defaults to '"'.

    Returns:
        success.html: showing success message in html page.
    """
    #print("I'm handling the submit")
    ans = []
    dataprepare = []
    if request.method == "POST":
        with open(f"polls/{pk1}/pattern.toml", mode="rb") as fp:
            questionsList = tomli.load(fp)

        if len(questionsList['questions'])>0:
            for l in range(1,len(questionsList['questions'])+1):
                value = questionsList["questions"][str(l)]['text'].split(' ')[0]
                optionss = request.form.getlist(value)
                g = ""
                for i in optionss:
                    g += i+" | "
                options = g.rstrip(" | ")
                if len(options) == 1:
                    options = request.form[value]
                ans.append(options)

        rangeV = len(ans)
        user_identity_number = user_id_number 
        user_id_number = user_id_number[:len(user_id_number)-1]
        try:
            with open('polls/'+str(pk1)+'/results.csv', 'r') as csvfile:     # writing to csv file answers stored with questions
                fieldnames = ['UUID']    
                dictData = {'UUID': user_identity_number}
            with open('polls/'+str(pk1)+'/results.csv', 'a') as csvfile:     # writing to csv file answers stored with questions
                fieldnames = ['UUID']    
                dictData = {'UUID': user_identity_number}
                # writer.writerow({'UUID': user_id_number})
                for i in range(rangeV):
                    questinDynamic = "question "+str(i+1)
                    fieldnames.append(questinDynamic)  
                    dictData[questinDynamic] = ans[i]
                writer = csv.writer(csvfile)#csv.DictWriter(csvfile, fieldnames=fieldnames)   
                # writer.writeheader()  
                ans.insert(0,user_identity_number)
                writer.writerow(ans)    
        except Exception as e:
            with open('polls/'+str(pk1)+'/results.csv', 'w') as csvfile:     # writing to csv file answers stored with questions
                fieldnames = ['UUID']    
                dictData = {'UUID': user_identity_number}
                # writer.writerow({'UUID': user_id_number})
                for i in range(rangeV):
                    questinDynamic = "question "+str(i+1)
                    fieldnames.append(questinDynamic)  
                    dictData[questinDynamic] = ans[i]
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)   
                writer.writeheader()  
                writer.writerow(dictData)  
    return render_template("success.html")


@app.route("/upload", methods=['GET', 'POST'])
def upload():
    """Function that manages the upload of a new poll."""

    res = "failed"
    if request.method == "POST":
        contacts_file = request.files['contacts_csv_file']
        if "pattern" in request.form:
            pattern = request.form['pattern']
            print("jj")
            poll_uuid = create_exist_poll(pattern, contacts_file)
               
        elif "toml_filename" in request.files:
            questions_file = request.files['toml_filename']
            print(type(questions_file))
            poll_uuid = create_new_poll(questions_file, contacts_file)
        # sending email from contacts csv file
        mail_response = send_emails(poll_uuid)
        res = "sent success"
        
    return render_template("success.html", result=res)


def sendMailUser(recieverMail,toData):
    """Mail sending function."""
    with open("smtp.toml", mode="rb") as fp:
        smtp_data = tomli.load(fp)
    port = smtp_data["port"]
    smtp_server = smtp_data["smtp_server"]
    sender_email = smtp_data["sender_email"]
    receiver_email = recieverMail
    password = smtp_data["password"]
    message = toData
    context = ssl.create_default_context()
    with smtplib.SMTP(smtp_server, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message) #email sending to and from mail 


def init_argparser():
    """Initialize the command line parser."""
    commandData = "echo pymailgen body.txt contacts.csv --ssmtp header.txt"
    return "parser"

#csv file reading 
def read_data_file(data_file=None, delimiter=',', quotechar='"'):
    """Loads the data used to fill the emails."""
    data = []
    with open("contacts.csv", 'r') as f:
        lines = csv.DictReader(f, delimiter=delimiter, quotechar=quotechar)
        for l in lines:
            data.append(l)
    return data

# email address validation with regex
def is_valid_address(email_address):
    """Check the validity of an email address."""
    if re.match(r"[^@]+@[^@]+\.[^@]+", email_address):
        return True
    else:
        return False


def create_new_poll(pattern_file, contacts_file):
    """_summary_
    Args:
        pattern_file (string): this file data is quetions
        contacts_file (string): this is file email id's and some data
    Returns:
        string: return poll uuid this is completely unique not same existing uuid's
    """
    contact_user_uuid = uuid.uuid4()
    contact_user_id = str(contact_user_uuid).split("-")[0]
    poll_uuid = uuid.uuid4()
    sub_dir = "polls/"
    new_poll_path = os.path.join(sub_dir, str(poll_uuid))
    os.mkdir(new_poll_path)
    contacts_file.save(os.path.join(new_poll_path, "contacts.csv"))
    pattern_file.save(os.path.join(new_poll_path, "pattern.toml"))
    return {"poll_uuid":poll_uuid,"contact_user_id":contact_user_id}

#existing poll using 
def create_exist_poll(pattern, contacts_file):
    """_summary_
    Args:
        pattern_file (string): this file data is quetions
        contacts_file (string): this is file email id's and some data
    Returns:
        string: return poll uuid this is completely unique not same existing uuid's
    """
    contact_user_uuid = uuid.uuid4()
    contact_user_id = str(contact_user_uuid).split("-")[0]
    poll_uuid = uuid.uuid4()
    sub_dir = "polls/"
    new_poll_path = os.path.join(sub_dir, str(poll_uuid))
    os.mkdir(new_poll_path)
    contacts_file.save(os.path.join(new_poll_path, "contacts.csv"))
    with open('example-questions/'+str(pattern)+'.toml', 'r') as pattern_file:
    # questions_file = pattern_file
    # with open('dog_breeds.txt', 'r') as reader:
    # Note: readlines doesn't trim the line endings
        dog_breeds = pattern_file.readlines()

    with open('polls/'+str(poll_uuid)+'/pattern.toml', 'w') as writer:
        # Alternatively you could use
        # writer.writelines(reversed(dog_breeds))

        # Write the dog breeds to the file in reversed order
        for breed in dog_breeds:
            writer.write(breed)
    return {"poll_uuid":poll_uuid,"contact_user_id":contact_user_id}


def process(email_text, data, send_line_template, poll_uuid):
    """Generates an email file for each item in the input data."""
    #print("I am here, finally!!!")
    # TODO: read the contacts from the poll_uuid/contacts.csv
    length_of_data = len(str(len(data)))
    email_filename_template = 'email_{{:0{}d}}.txt'.format(length_of_data)
    # gets an empty sender script file
    with open(sender_script_filename, 'w') as f:
        pass
    i = 0

    sub_dir = "polls/"+str(poll_uuid['poll_uuid'])
    folder_name = "emails"
    path2 = os.path.join(sub_dir, folder_name)
    os.mkdir(path2)
    for data_index, item in enumerate(data):
        contact_user_uuid = uuid.uuid4()
        contact_user_id = str(contact_user_uuid).split("-")[0]
        
        email_filename_template = 'polls/'+str(poll_uuid['poll_uuid'])+'/emails/email_{uidddNo}.txt'.format(uidddNo=str(contact_user_id)+str(data_index))
        user_list_details = [str(contact_user_id)+ str(data_index), item['Email'], item['Name'], item['FamilyName']]
        user_csv = user_contacts_csv(user_list_details,poll_uuid['poll_uuid'])
        
        #user questions url generated here
        url_link = "http://localhost:5000/poll/"+str(poll_uuid['poll_uuid'])+"/"+str(contact_user_id)+str(data_index) 
        if 'Blacklist' in item:
            if item['Blacklist'] != '':
                continue
        item['url'] = url_link
        email = email_text.format(**item)
        email_filename = email_filename_template.format(i)
        addr = item['Email'] 
        if not is_valid_address(addr):
            #print('Invalid email address "{}" at line {}'.format(addr, data_index + 1))
            continue
        with open(email_filename, 'w') as f:
            f.write(email)
        fields = {'Address': addr, 'EmailFName': email_filename}
        send_Mail = sendMailUser(addr,email)
        send_line = send_line_template.format(**fields)
        
        with open(sender_script_filename, 'a') as f:
            f.write(send_line + '\n')
            f.write('echo "[$(date +\"%Y-%m-%d %T.%3N\")] Email #{} to {}" | tee -a auto_mailer.log'.format(i, addr) + '\n')
        sendall = "echo bash sendall.sh" 
        i += 1
        

def check_data(data):
    if len(data) < 1:
        #print('Data file must contain at least one row.')
        sys.exit(1)
    if 'Email' not in data[0]:
        #print('Data file must contain the "Email" column (case-sensitive).')
        sys.exit(1)


def send_emails(poll_uuid):
    data = read_data_file() #read_data_file(args.datafile)
    check_data(data)

    with open("body.txt", 'r') as f:
        body = f.read()
    headerss = open("header.txt", 'r')
    if  headerss is not None:
        with open("header.txt", 'r') as f:
            header = f.read()
        email_text = header + '\n' + body
        send_line_template = 'ssmtp -t < {EmailFName}'
    else:
        email_text = body
    process(email_text, data, send_line_template, poll_uuid)


def main():
    # parser = init_argparser()
    # args = parser.parse_args()
    app.run(debug=True, port=5000)


if __name__ == "__main__":
    main()
